## ReactJS UI for test traffic lights SaaS

### Clone the repo

```
git clone https://bitbucket.org/vageesh/test-react-ui/src/master/

npm install

npm start
```
You should see the app in your browser address bar at [http://localhost:3000](http://localhost:3000)

![Demo UI interaction]
(public/ezgif.com-optimize.gif)